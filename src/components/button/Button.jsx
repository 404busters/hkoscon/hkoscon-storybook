import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Button({ children, className }) {
  const classes = classNames('btn', className);
  return (
    <button type="button" className={classes}>
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Button.defaultProps = {
  className: '',
};
