import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

export default function PrimaryButton({ children }) {
  return (
    <Button className="primary secondary-text">
      { children }
    </Button>
  );
}

PrimaryButton.propTypes = {
  children: PropTypes.node.isRequired,
};
