import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';
import PrimaryButton from '../src/components/button/PrimaryButton';
import SecondaryButton from '../src/components/button/SecondaryButton';

storiesOf('Components', module)
  .addDecorator(withKnobs)
  .add('Button', () => {
    const label = 'Color';
    const options = {
      Primary: 'primary',
      Secondary: 'secondary',
    };
    const value = select(label, options, 'primary');
    if (value === 'primary') {
      return (
        <PrimaryButton>
          Testing
        </PrimaryButton>
      );
    }
    return (
      <SecondaryButton>
          Testing
      </SecondaryButton>
    );
  });
